import sympy as sym 
from sympy.vector import CoordSys3D, Del 
import sympy.plotting as symplt 
from sympy.functions.special.tensor_functions import KroneckerDelta 
from sympy.plotting import plot, plot3d
import numpy as np 
import matplotlib
# matplotlib.use('agg') # To avoid RuntimeError: Invalid DISPLAY variable
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import os
import sys



# ============= Usage ====================== 
# 
# python pol_tor_rep.py n m type_PT 
# 
#   n: degree 
#   m: order 
#   type_PT: pol, tor, total 
# 
# ============= Parameters ================= 
pnm = [1, 1] 
tnm = [1, 1] 
deg_sep = 1

n = 1
m = 1
type_PT = 'pol'
n = int(sys.argv[1]) 
m = int(sys.argv[2]) 
type_PT = str(sys.argv[3]) 

cwd = os.getcwd()
dir_name = '/fig_quiver3d/'
# ==========================================





# sym.init_printing() 
delop = Del() 
c = CoordSys3D('c')
s = CoordSys3D('s', transformation='spherical')
# n, m = sym.symbols('n, m')
r, theta, phi = sym.symbols('r, theta, phi', real=True)



# =================================Poloidal part ==================================
# ======= Without r dep.==================
pnm_re, pnm_im = sym.symbols('pnm_re, pnm_im', real=True) 
# ========================================
# pnm_re = sym.Function('P_nm_re', real=True)(s.r)
# pnm_im = sym.Function('P_nm_im', real=True)(s.r)

Pol = ((pnm_re+sym.I*pnm_im)*sym.Ynm(n, m, s.theta, s.phi) + \
      (-1)**m*(pnm_re-sym.I*pnm_im)*sym.Ynm(n, -m, s.theta, s.phi) ) / (1+KroneckerDelta(m,0))
Pol = Pol.expand(func=True)

# ======= Check Poloidal part ============
# tmp = sym.re(Pol) + sym.im(Pol)*sym.I
# display('Pol =', tmp.subs({s.r: r, s.theta: theta, s.phi: phi}))
# ========================================

B_Pol = delop.cross(delop.cross(Pol*s.r*s.i)).doit().subs({s.r: r, s.theta: theta, s.phi: phi})
# B_Pol = sym.simplify(B_Pol.expand(func=True)) # Not necessary ............ 

# ============== Note ====================
# # Notice that the vector s.i is not defined as real. 
# # To simplify exp(I*theta), one need to do it by components. 
# # Also notice that derivative is not defined for real or imaginary ... 
# ========================================

# ======== Check if you want =============
# if sym.im(B_Pol.dot(s.i)) != 0 or sym.im(B_Pol.dot(s.j)) != 0 or sym.im(B_Pol.dot(s.k)) != 0: 
#     print('Imaginary part check: ')
#     print(sym.im(B_Pol.dot(s.i)))
#     print(sym.im(B_Pol.dot(s.j)))
#     print(sym.im(B_Pol.dot(s.k)))
# ========================================

B_Pol_si = sym.simplify(sym.re( B_Pol.dot(s.i) ))
B_Pol_sj = sym.simplify(sym.re( B_Pol.dot(s.j) ))
B_Pol_sk = sym.simplify(sym.re( B_Pol.dot(s.k) ))
B_Pol = B_Pol_si*s.i + B_Pol_sj*s.j + B_Pol_sk*s.k
# display('B_Pol = ', B_Pol)

B_Pol_c = B_Pol.subs(\
      {s.i: sym.sin(theta)*sym.cos(phi)*c.i+sym.sin(theta)*sym.sin(phi)*c.j+sym.cos(theta)*c.k, \
      s.j: sym.cos(theta)*sym.cos(phi)*c.i+sym.cos(theta)*sym.sin(phi)*c.j-sym.sin(theta)*c.k, \
      s.k: -sym.sin(phi)*c.i+sym.cos(phi)*c.j})
B_Pol_x = sym.lambdify([r, theta, phi], B_Pol_c.dot(c.i).subs({pnm_re:pnm[0], pnm_im:pnm[1]}))
B_Pol_y = sym.lambdify([r, theta, phi], B_Pol_c.dot(c.j).subs({pnm_re:pnm[0], pnm_im:pnm[1]}))
B_Pol_z = sym.lambdify([r, theta, phi], B_Pol_c.dot(c.k).subs({pnm_re:pnm[0], pnm_im:pnm[1]}))

# ==================================================================================




# ================================= Toroidal part ==================================
# =============== Note ===================
# Pol. and tor. parts are similar, so additional notes will be discarded here. 
# ========================================
tnm_re, tnm_im = sym.symbols('T_nm_re, T_nm_im', real=True) 
# tnm_re = sym.Function('T_nm_re', real=True)(s.r)
# tnm_im = sym.Function('T_nm_im', real=True)(s.r)

Tor = ((tnm_re+sym.I*tnm_im)*sym.Ynm(n, m, s.theta, s.phi) + \
      (-1)**m*(tnm_re-sym.I*tnm_im)*sym.Ynm(n, -m, s.theta, s.phi) ) / (1+KroneckerDelta(m,0))
Tor = Tor.expand(func=True)

# ======= Check Toroidal part ============
# tmp = sym.re(Tor) + sym.im(Tor)*sym.I
# display('Tor =', tmp.subs({s.r: r, s.theta: theta, s.phi: phi}))
# ========================================

B_Tor = delop.cross(Tor*s.r*s.i).doit().subs({s.r: r, s.theta: theta, s.phi: phi})
# B_Tor = sym.simplify(B_Tor.expand(func=True)) # Not necessary ............ 

# ======== Check if you want =============
# if sym.im(B_Pol.dot(s.i)) != 0 or sym.im(B_Pol.dot(s.j)) != 0 or sym.im(B_Pol.dot(s.k)) != 0: 
#     print('Imaginary part check: ')
#     print(sym.im(B_Pol.dot(s.i)))
#     print(sym.im(B_Pol.dot(s.j)))
#     print(sym.im(B_Pol.dot(s.k)))
# ========================================

B_Tor_si = sym.simplify(sym.re( B_Tor.dot(s.i) ))
B_Tor_sj = sym.simplify(sym.re( B_Tor.dot(s.j) ))
B_Tor_sk = sym.simplify(sym.re( B_Tor.dot(s.k) ))
B_Tor = B_Tor_si*s.i + B_Tor_sj*s.j + B_Tor_sk*s.k
# display('B_Tor = ', B_Tor)

B_Tor_c = B_Tor.subs(\
      {s.i: sym.sin(theta)*sym.cos(phi)*c.i+sym.sin(theta)*sym.sin(phi)*c.j+sym.cos(theta)*c.k, \
      s.j: sym.cos(theta)*sym.cos(phi)*c.i+sym.cos(theta)*sym.sin(phi)*c.j-sym.sin(theta)*c.k, \
      s.k: -sym.sin(phi)*c.i+sym.cos(phi)*c.j})
B_Tor_x = sym.lambdify([r, theta, phi], B_Tor_c.dot(c.i).subs({tnm_re:tnm[0], tnm_im:tnm[1]}))
B_Tor_y = sym.lambdify([r, theta, phi], B_Tor_c.dot(c.j).subs({tnm_re:tnm[0], tnm_im:tnm[1]}))
B_Tor_z = sym.lambdify([r, theta, phi], B_Tor_c.dot(c.k).subs({tnm_re:tnm[0], tnm_im:tnm[1]}))

# ==================================================================================



r = 1
theta = np.linspace(0, np.pi, 10)
phi = np.linspace(0, 2*np.pi, 2*10)
rv, thetav, phiv = np.meshgrid(r, theta, phi)

x = rv * np.sin(thetav) * np.cos(phiv) 
y = rv * np.sin(thetav) * np.sin(phiv) 
z = rv * np.cos(thetav) 


print(type_PT)
if type_PT == 'pol': 
      print('poloidal')
      B_x = B_Pol_x(rv, thetav, phiv)
      B_y = B_Pol_y(rv, thetav, phiv)
      B_z = B_Pol_z(rv, thetav, phiv)
elif type_PT == 'tor': 
      print('toroidal')
      B_x = B_Tor_x(rv, thetav, phiv)
      B_y = B_Tor_y(rv, thetav, phiv)
      B_z = B_Tor_z(rv, thetav, phiv)
elif type_PT == 'total': 
      B_x = B_Pol_x(rv, thetav, phiv) + B_Tor_x(rv, thetav, phiv)
      B_y = B_Pol_y(rv, thetav, phiv) + B_Tor_y(rv, thetav, phiv)
      B_z = B_Pol_z(rv, thetav, phiv) + B_Tor_z(rv, thetav, phiv)



fig = plt.figure()
ax = fig.gca(projection='3d')

plt.title('n =%2d, m =%2d, '%(n,m)+type_PT)
plt.xlabel('x')
plt.ylabel('y')

ax.quiver(x, y, z, B_x, B_y, B_z, length=0.3, normalize=True)


title_str = 'n%02d_m%02d_'%(n,m)+type_PT
if not os.path.exists(cwd+dir_name+title_str): 
	os.makedirs(cwd+dir_name+title_str) 


for angle in range(0, 360, deg_sep): 
      print(angle)
      ax.view_init(30, angle)
      plt.savefig(cwd+dir_name+title_str+'/foo_%03d.jpg'%angle)

