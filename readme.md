# Poloidal Toroidal Decomposition 
This project aims at visualizing the poloidal and toroidal parts of a divergence-free vector field on a sphere. The two parts can be further decomposed by Spherical Harmonics (SH) and Chebyshev Polynomials (e.g., Triana et al., 2019, GJI). Each degree and order of the expansion is plotted accordingly. 


### Required module 
* sympy 
* imageio or Pillow (PIL) 


### Usage 
```
# Generate a series of figures for a movie 
python gen_quiver3d.py n m type_PT 

# Assemble all the figures and make them into a move 
python jpg2gif.py path fps 
```


### Notes 
SH components share the same symmetry relation as SH, i.e., _[n,-m] = (-1)**m * [n,m]_
