import imageio 
from PIL import Image # alternative 
import os 
import sys 

"""
Example: 

python jpg2gif.py ~\\Poloidal_Toroidal_Decomposition\\fig_xxx\\ 10
                  ^path to where figures are stored             ^fps 

"""

path = str(sys.argv[1]) 

# ========== imageio =================
images = [] 
for file in os.listdir(path): 
	if (file != 'movie.gif'): 
		images.append( imageio.imread(path+file) )
		print(file)
imageio.mimsave(path+'movie.gif', images, fps=sys.argv[2])


# =========== PIL ====================
# images = [] 
# for file in sorted(os.listdir(path)): 
# 	if (file != 'movie.gif'): 
# 		images.append( Image.open(path+file) )
# 		print(file)
# fps = int(sys.argv[2])
# images[0].save(path+'movie.gif', save_all=True, append_images=images[1:], \
# 	           optimize=False, duration=1000./fps, loop=0)
